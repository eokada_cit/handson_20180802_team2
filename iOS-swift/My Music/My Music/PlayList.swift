//
//  PlayList.swift
//  My Music
//
//  Created by André Haas on 02/08/2018.
//  Copyright © 2018 My Company. All rights reserved.
//

import Foundation

public class PlayList: Codable {
    public let id: String
    public let playlistMusicas: [PlayListMusic]
    public let usuario: Usuario
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case playlistMusicas = "playlistMusicas"
        case usuario = "usuario"
    }
    
    public init(id: String, playlistMusicas: [PlayListMusic], usuario: Usuario) {
        self.id = id
        self.playlistMusicas = playlistMusicas
        self.usuario = usuario
    }
}

