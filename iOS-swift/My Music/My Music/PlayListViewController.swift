//
//  PlayListViewController.swift
//  My Music
//
//  Created by André Haas on 02/08/2018.
//  Copyright © 2018 My Company. All rights reserved.
//

import UIKit

class PlayListViewController: UIViewController {
    
    var usuario : String!
    var listMusica : [Musica] = []

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        setupTable()
        self.tableView.reloadData()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = usuario
    }
    
    func setupTable(){
        tableView.register(UINib(nibName: "MusicTableViewCell", bundle: nil), forCellReuseIdentifier: "cellID")
        tableView.delegate = self
        tableView.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func removeMusica(sender: UIGestureRecognizer){
        let index = sender.view!.tag
        listMusica.remove(at: index)
        tableView.reloadData()
    }
    
    func loadData(){
        
        for i in 1..<50 {
            let musica = Musica(id: "musica_\(i)", nome: "musica_\(i)", artistaID: "artista_\(i)", artista: Artista(id: "artista_\(i)", nome: "artista_nome_\(i)"))
        self.listMusica.append(musica)
        }
        
      
    }


}

extension PlayListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listMusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let musica = listMusica[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID") as! MusicTableViewCell
        cell.artistName.text = musica.artista.nome
        cell.musicName.text = musica.nome
        cell.selectionStyle = .none
       // cell.buttonRemove = UIButton()
        let tap = UITapGestureRecognizer(target: self, action: #selector(removeMusica(sender:)))
        tap.numberOfTapsRequired = 1
      
        cell.buttonRemove.addGestureRecognizer(tap)
       cell.buttonRemove.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105.0
    }
}
