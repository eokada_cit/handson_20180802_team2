//
//  Artista.swift
//  My Music
//
//  Created by André Haas on 02/08/2018.
//  Copyright © 2018 My Company. All rights reserved.
//

import Foundation

public class Artista: Codable {
    public let id: String
    public let nome: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case nome = "nome"
    }
    
    public init(id: String, nome: String) {
        self.id = id
        self.nome = nome
    }
}
