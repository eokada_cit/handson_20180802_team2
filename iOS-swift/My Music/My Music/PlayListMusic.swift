//
//  PlayListMusic.swift
//  My Music
//
//  Created by André Haas on 02/08/2018.
//  Copyright © 2018 My Company. All rights reserved.
//

import Foundation

public class PlayListMusic: Codable {
    public let playlistID: String
    public let musicaID: String
    public let musica: Musica
    
    enum CodingKeys: String, CodingKey {
        case playlistID = "playlistId"
        case musicaID = "musicaId"
        case musica = "musica"
    }
    
    public init(playlistID: String, musicaID: String, musica: Musica) {
        self.playlistID = playlistID
        self.musicaID = musicaID
        self.musica = musica
    }
}
