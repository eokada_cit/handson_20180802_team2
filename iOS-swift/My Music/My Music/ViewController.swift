//
//  ViewController.swift
//  My Music
//
//  Created by dheos on 01/07/18.
//  Copyright © 2018 My Company. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func Login(_ sender: Any) {
        
        if !(userName.text?.isEmpty)! {
            
            performSegue(withIdentifier: "seguePlayList", sender: nil)
            
//            let story = UIStoryboard(name: "Main", bundle: nil)
//            let view = story.instantiateViewController(withIdentifier: "PlayList") as! PlayListViewController
//            view.usuario = userName.text
//
//            present(view, animated: true, completion: nil)
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePlayList"{
            let svc = segue.destination as! PlayListViewController
            svc.usuario = userName.text
        }
    }
 
    
}

