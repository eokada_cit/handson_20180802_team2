//
//  Musica.swift
//  My Music
//
//  Created by André Haas on 02/08/2018.
//  Copyright © 2018 My Company. All rights reserved.
//

import Foundation

public class Musica: Codable {
    public let id: String
    public let nome: String
    public let artistaID: String
    public let artista: Artista
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case nome = "nome"
        case artistaID = "artistaId"
        case artista = "artista"
    }
    
    public init(id: String, nome: String, artistaID: String, artista: Artista) {
        self.id = id
        self.nome = nome
        self.artistaID = artistaID
        self.artista = artista
    }
}

