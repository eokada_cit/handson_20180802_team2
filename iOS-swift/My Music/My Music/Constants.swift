//
//  Constants.swift
//  My Music
//
//  Created by André Haas on 02/08/2018.
//  Copyright © 2018 My Company. All rights reserved.
//

import Foundation

struct Constants {
    struct urls {
        static var urlAPI = "http://localhost:5000/api/"
    }
}
