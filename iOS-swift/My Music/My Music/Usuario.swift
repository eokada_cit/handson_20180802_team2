//
//  Usuario.swift
//  My Music
//
//  Created by André Haas on 02/08/2018.
//  Copyright © 2018 My Company. All rights reserved.
//

import Foundation

public class Usuario: Codable {
    public let id: String
    public let nome: String
    public let playlistID: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case nome = "nome"
        case playlistID = "playlistId"
    }
    
    public init(id: String, nome: String, playlistID: String) {
        self.id = id
        self.nome = nome
        self.playlistID = playlistID
    }
}
