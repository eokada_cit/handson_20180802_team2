//
//  ApiMusic.swift
//  My Music
//
//  Created by André Haas on 02/08/2018.
//  Copyright © 2018 My Company. All rights reserved.
//

import Foundation
import Alamofire

enum ApiMusic : URLRequestConvertible{
   
    case getListaMuscias([String : Any])
    case getPlayListByUser([String :  Any])
    case addMusicPlayList(String, [String:Any])
    case deleteMusicaPlayList(String, String)
   
    var method : Alamofire.HTTPMethod {
        switch self {
        case .getListaMuscias(_):
            return .get
        case .getPlayListByUser(_):
            return .get
            
        case .addMusicPlayList(_ ,_ ):
            return .put
        case .deleteMusicaPlayList(_,_):
            return .delete
        }
    }
    
    var path : String {
        switch self {
        case .getListaMuscias(_):
            return "musicas"
         case .getPlayListByUser:
            return "playlists"
        case .addMusicPlayList(let playlistId,_ ):
            return "playlists/\(playlistId)/musicas"
        case .deleteMusicaPlayList(let playListId,let musicaID):
            return "playlists/\(playListId)/musicas/{\(musicaID)"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let uRL = NSURL(string: Constants.urls.urlAPI)!
        var urlRequest = URLRequest(url: uRL.appendingPathComponent(path)!)
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getListaMuscias(let parameters):
           return try Alamofire.URLEncoding.default.encode(urlRequest, with: parameters)
            
        case .getPlayListByUser(let parameters):
           return try Alamofire.URLEncoding.default.encode(urlRequest, with: parameters)
        case .addMusicPlayList(_,let musica):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: musica)
        case .deleteMusicaPlayList(_, _):
             return try Alamofire.URLEncoding.default.encode(urlRequest, with: nil)
        }
    }
    
    
    
}

